from dpp.connect import get_snowflake_options
import snowflake.connector


def query():
    snowflake_options = get_snowflake_options(database='<DATABASE>',
                                              schema='<SCHEMA>',
                                              role='<ROLE>',
                                              client_id="<CLIENT_ID>",
                                              client_secret="<CLIENT_SECRET>")

    ctx = snowflake.connector.connect(**snowflake_options)
    cs = ctx.cursor()

    try:
        cs.execute("SELECT * FROM <TABLE>")
        one_row = cs.fetchone()
        print(one_row[0])

    finally:
        cs.close()

    ctx.close()


query()
